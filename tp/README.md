# TP B1 Workstation

* [TP 1](./1)

## Rendu de TP 

### Format de rendu 

Le rendu de TP **DOIT** se faire *via* un dépôt git, au format Markdown.  

TOUTES les opérations pour réaliser ce qui est demandé doivent figurer dans le rendu de TP :
* au format Markdown
* ou dans un script (Vagrantfile, scripts shell, etc.)

Les points précédés d'un 🌞 doivent **obligatoirement** figurer dans le rendu.  
Les points précédés d'un 🐙 sont des bonus, ils sont facultatifs.

### Modalités de rendu

Le rendu de TP se fait *via* Discord, en m'envoyant un message privé au format `nom,url_du_dépôt_git`.  
Par exemple : `macron,https://gitlab.inria.fr/stopcovid19/accueil`.

**Un rendu par TP et par personne.**
